﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetWindowsLogs
{
    class Program
    {
        static void Main(string[] args)
        {
            //"Application"应用程序, "Security"安全, "System"系统
            EventLog eventlog = new EventLog();
            eventlog.Log = "Security";
            EventLogEntryCollection eventLogEntryCollection = eventlog.Entries;
            StringBuilder sb = new StringBuilder();
            string temp = string.Empty;
            var list = new List<EventLogEntry>();
            foreach (EventLogEntry entry in eventLogEntryCollection)
            {
                list.Add(entry);
            }
            list = list.Where(t => (DateTime.Now - t.TimeGenerated).Days >= 1).ToList();
            var days = from day in list
                       group day by day.TimeGenerated.Date into dayGroup
                       select new
                       {
                           Date = dayGroup.Key,
                           Shangban = dayGroup.Min(t => t.TimeGenerated),
                           Xiaban = dayGroup.Max(t => t.TimeGenerated),
                           Diff = (dayGroup.Max(t => t.TimeGenerated) - dayGroup.Min(t => t.TimeGenerated)).TotalHours
                       };
            var month = from m in days
                        group m by m.Date.Month into monthGroup
                        select new
                        {
                            Month = monthGroup.Key,
                            WorkDay = monthGroup.Count(t => t.Date.Month == monthGroup.Key),
                            Hours = (monthGroup.Sum(t => t.Diff)).ToString("0.##"),
                            Overtime = monthGroup.Count(t => t.Xiaban.Hour >= 21),
                            Dinner = monthGroup.Count(t => t.Xiaban.TimeOfDay > new TimeSpan(20, 30, 0)),
                            Days = monthGroup.ToList()
                        };

            foreach (var item in month)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Write(item, 1);
                Console.ResetColor();

                foreach (var day in item.Days)
                {
                    if (day.Xiaban.Hour >= 21)//有加班超过21点的，字体颜色为蓝色
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        if (day.Xiaban.Minute >= 30)//加班超过21：30的，字体颜色为绿色
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                        }
                    }
                    else if (day.Xiaban.TimeOfDay > new TimeSpan(20, 30, 0))//有加班餐补的，字体颜色为黄色
                        Console.ForegroundColor = ConsoleColor.Yellow;

                    Console.WriteLine("{0}  {1} {2} {3} {4}",
                        day.Date.ToString("yyyy-MM-dd"),
                        day.Shangban.ToString("HH:mm"),
                        day.Xiaban.ToString("HH:mm"),
                        day.Diff.ToString("0.##"),
                        day.Date.ToString("dddd", new System.Globalization.CultureInfo("zh-cn"))
                        );

                    Console.ResetColor();
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Write(item, 2);
                Console.ResetColor();

                Console.WriteLine();
            }
            Console.ReadKey();
        }

        private static void Write(dynamic obj, int type = 0)
        {
            if (type == 1)
                Console.WriteLine("/*******************************************************************************");
            Console.WriteLine("{0}月，上班：{1}d，{2}h，加班：{3}d（超过21点，蓝色、绿色）,加班餐：{4}d(彩色)",
                obj.Month, obj.WorkDay, obj.Hours, obj.Overtime, obj.Dinner);
            if (type == 2)
                Console.WriteLine("*******************************************************************************/");
        }
    }
}